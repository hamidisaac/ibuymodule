const countDownTimer = (duration, showId)=>
{
    duration = duration * 60;
    var timer = duration, minutes, seconds;
    var interval = setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        $("#"+ showId).html(minutes + ":" + seconds);

        if (--timer < 0) {
            clearInterval(interval)
        }
    }, 1000);
};


const sendSms = ()=>
{
    countDownTimer(2,'countDownTimer');
    $('.send-sms-btn').prop('disabled',true);

    $.ajax({
        type: 'POST',
        headers: {"cache-control": "no-cache"},
        url: ibuy_ajax_url,
        async: true,
        cache: false,
        data: {
            "ajax": 1,
            "action": "send-sms",
            "phone": $('.send-sms-box').find('[name="phone"]').val(),
        },
        success: function (result)
        {
            console.log(result);
            $('.send-sms-btn').prop('disabled',false);
            var data = JSON.parse(result);
            console.log(data);
            if(data.errors.length){
                console.log(data.errors);
            }else{
                $('.send-sms-box').hide();
                if(data.register_form === false){
                    $('.verification-box').find('#form_input_firstname').hide();
                    $('.verification-box').find('#form_input_lastname').hide();
                }
                $('.verification-box').show();
                return 1;
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.send-sms-btn').prop('disabled',true);
            console.log("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
        }
    });

    return 0;
};

$(document).ready(function() {
    var send_sms = 0;
    $('.send-sms-btn').click(function (e) {
        console.log(send_sms)
        if (send_sms) return;
        e.preventDefault();
        send_sms = 1;
        sendSms();
    });
    $("#countDownTimer").bind("DOMSubtreeModified",() => {
        if($("#countDownTimer").html() == "00:00")
        {
            $("#countDownTimer").html('02:00').hide();
            $("#re-send-sms").show();
        }
    });
    $("#re-send-sms").click(() => {
        $("#countDownTimer").show();
        $("#re-send-sms").hide();
        sendSms();
    });


});