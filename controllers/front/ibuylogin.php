<?php
/**
 * 2007-2015 Leotheme
 *
 * NOTICE OF LICENSE
 *
 * Leo Quick Login And Social Login
 *
 * DISCLAIMER
 *
 *  @author    leotheme <leotheme@gmail.com>
 *  @copyright 2007-2015 Leotheme
 *  @license   http://leotheme.com - prestashop template provider
 */

use PrestaShop\PrestaShop\Core\Crypto\Hashing;

class IbuyMarketIbuyLoginModuleFrontController extends ModuleFrontController
{

    public $php_self;
    public function sendSms(Customer $customer)
    {
        $random = rand(100000,999999);

        $client = new \SoapClient('http://ws2.adpdigital.com/services/MessagingService?wsdl');

        $result = $client->send([
            'userName' => 'parnian4030',
            'password' => 'parnian4030',
            'destNo' => (int)('98' . ltrim($customer->phone,0)),
            'sourceNo' => 98200004030,
            'sourcePort' => null,
            'destPort' => null ,
            'clientId' => 1 ,
            'messageType' => 1,
            'encoding' => 2,
            'longSupported' => 0,
            'dueTime' => time(),
            'content' => " به IBuyMarket خوش آمدید \n کد ورود : " .$random,
        ]);


        $customer->passwd = $random;

        return $customer->update();
        return 1;
    }
    public function displayAjax()
    {
        $context = Context::getContext();
        $action = Tools::getValue('action');

        $array_result = [];
        $errors = [];
        $success = [];
        $register_form = false;


        if ($action == 'send-sms') {

            $phone = Tools::getValue('phone');

            if (!Validate::isPhoneNumber($phone) || empty($phone)) {
                $errors[] = $this->l('Invalid phone number', 'ibuymarket');
            } else {
                $customer = new Customer($context->customer->id);
                $customer->getByPhone($phone);
                if(!isset($customer->id))
                {
                    $customer = new Customer($context->customer->id);
                    $customer->phone = $phone;
                    $customer->is_guest = 0;
                    $customer->add();
                }
                if(empty($customer->firstname) || empty($customer->lastname))
                {
                    $register_form = true;
                }

                if($this->sendSms($customer)){
                    $success[] = 'code has ben send';
                }
            }
        }
        $array_result['success'] = $success;
        $array_result['errors'] = $errors;
        $array_result['register_form'] = $register_form;
        die(Tools::jsonEncode($array_result));
    }

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        $this->php_self = 'leocustomer';

        if (Tools::getValue('ajax')) {
            return;
        }
        parent::initContent();
    }
}
