<?php
use Symfony\Component\Translation\TranslatorInterface;

class CustomerLoginFormatter extends CustomerLoginFormatterCore
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
        parent::__construct($translator);
    }

    public function getFormat()
    {









        return [
            'back' => (new FormField())
                ->setName('back')
                ->setType('hidden'),
            'phone' => (new FormField())
                ->setName('phone')
                ->setType('text')
                ->setRequired(true)
                ->setAvailableValues(['placeholder' => '09** *** ****'])
                ->setLabel($this->translator->trans(
                    'Phone Number',
                    [],
                    'Shop.Forms.Labels'
                ))->addConstraint('isPhoneNumber'),
            'code' => (new FormField())
                ->setName('code')
                ->setAvailableValues(['placeholder' => '- - - - -'])
                ->setLabel(
                    $this->translator->trans(
                        'کد تایید را وارد کنید',
                        [],
                        'Shop.Forms.Labels'
                    )
                ),
            'firstname' => (new FormField())
                ->setName('firstname')
                ->setAvailableValues(['placeholder' => 'نام'])
                ->setLabel(
                    $this->translator->trans(
                        'مشخصات',
                        [],
                        'Shop.Forms.Labels'
                    )
                ),
            'lastname' => (new FormField())
                ->setName('lastname')
                ->setAvailableValues(['placeholder' => 'نام'])
                ->setAvailableValues(['placeholder' => 'نام خانوادگی'])
                ->setRequired(false)
        ];


    }
}
