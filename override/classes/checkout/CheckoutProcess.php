<?php

use PrestaShop\PrestaShop\Core\Foundation\Templating\RenderableProxy;

class CheckoutProcess extends CheckoutProcessCore
{

    public function render(array $extraParams = array())
    {
        $params = array(
            'steps2' => array_map(function (CheckoutStepInterface $step) {
                return array(
                    'identifier' => $step->getIdentifier(),
                    'title' => $step->getTitle(),
                    'complete' => $step->isComplete(),
                    'current' => $step->isCurrent(),
                );
            }, $this->getSteps()),
        );
        return parent::render($params);
    }

}
