<?php

use PrestaShop\PrestaShop\Adapter\CoreException;
use PrestaShop\PrestaShop\Adapter\ServiceLocator;

class Customer extends CustomerCore
{
    public $phone;

    public function __construct($id = null)
    {
        parent::$definition['fields']['phone'] = ['type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'required' => false, 'size' => 255];
        parent::$definition['fields']['lastname'] = ['type' => self::TYPE_STRING, 'validate' => 'isCustomerName', 'required' => false, 'size' => 255];
        parent::$definition['fields']['firstname'] = ['type' => self::TYPE_STRING,'required' => false, 'size' => 255];
        parent::$definition['fields']['email'] = ['type' => self::TYPE_STRING, 'validate' => 'isEmail', 'required' => false, 'size' => 255];
        parent::$definition['fields']['passwd'] = ['type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'required' => false, 'size' => 255];
        parent::__construct($id);
    }

    public function getByPhone($phone, $plaintextPassword = null)
    {

        if (!Validate::isPhoneNumber($phone) || empty($phone)) {
            die(Tools::displayError());
        }

        $shopGroup = Shop::getGroupFromShop(Shop::getContextShopID(), false);

        $sql = new DbQuery();
        $sql->select('c.`passwd`');
        $sql->from('customer', 'c');
        $sql->where('c.`phone` = \'' . pSQL($phone) . '\'');
        if (Shop::getContext() == Shop::CONTEXT_SHOP && $shopGroup['share_customer']) {
            $sql->where('c.`id_shop_group` = ' . (int) Shop::getContextShopGroupID());
        } else {
            $sql->where('c.`id_shop` IN (' . implode(', ', Shop::getContextListShopID(Shop::SHARE_CUSTOMER)) . ')');
        }

        $sql->where('c.`deleted` = 0');

        $password = Db::getInstance()->getValue($sql);

        if (!is_null($plaintextPassword) && $plaintextPassword != $password) {
            return false;
        }

        $sql = new DbQuery();
        $sql->select('c.*');
        $sql->from('customer', 'c');
        $sql->where('c.`phone` = \'' . pSQL($phone) . '\'');
        if (Shop::getContext() == Shop::CONTEXT_SHOP && $shopGroup['share_customer']) {
            $sql->where('c.`id_shop_group` = ' . (int) Shop::getContextShopGroupID());
        } else {
            $sql->where('c.`id_shop` IN (' . implode(', ', Shop::getContextListShopID(Shop::SHARE_CUSTOMER)) . ')');
        }
        $sql->where('c.`deleted` = 0');

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);

        if (!$result) {
            return false;
        }

        $this->id = $result['id_customer'];
        foreach ($result as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }

        return $this;
    }

    public static function getCustomerByPhone($phone)
    {
        if (!Validate::isPhoneNumber($phone) || empty($phone)) {
            return [];
        }
        $sql = 'SELECT *
                FROM `' . _DB_PREFIX_ . 'customer`
                WHERE `phone` = \'' . pSQL($phone) . '\'
                    ' . Shop::addSqlRestriction(Shop::SHARE_CUSTOMER);

        return Db::getInstance()->executeS($sql);
    }

}
